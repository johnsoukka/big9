import { GuardedPrediction, Ticket, WildCard } from "../../model";
import { ticketsCost } from "..";

describe("ticketsCost", () => {
  it("should calculate cost on tickets", () => {
    const tickets: Ticket[] = [
      [
        GuardedPrediction({
          matchNo: 1,
          guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
        }),
        GuardedPrediction({
          matchNo: 2,
          guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
        }),
        WildCard({ matchNo: 3 }),
      ],
      [
        GuardedPrediction({
          matchNo: 1,
          guard: { fullTimeBets: ["1", "2"], overUnders: ["O"] },
        }),
        GuardedPrediction({
          matchNo: 2,
          guard: { fullTimeBets: ["X", "2"], overUnders: ["O", "U"] },
        }),
        GuardedPrediction({
          matchNo: 3,
          guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
        }),
      ],
    ];

    expect(ticketsCost(tickets)).toBe(3000);
  });
});
