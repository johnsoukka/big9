import { isGuardedPrediction, Ticket, TicketPrediction } from "../model";
import { guardToOutcomes } from "../reduction/linesToTickets";

const costPerLine = 50;

export function ticketsCost(tickets: Ticket[]): number {
  return tickets.reduce((sum: number, ticket: Ticket) => {
    return sum + ticketCost(ticket);
  }, 0);
}

export function ticketCost(ticket: Ticket): number {
  const combinations = ticket.reduce(
    (product: number, prediction: TicketPrediction) => {
      if (isGuardedPrediction(prediction)) {
        return product * guardToOutcomes(prediction.guard).length;
      } else {
        return product;
      }
    },
    1
  );

  return combinations * costPerLine;
}
