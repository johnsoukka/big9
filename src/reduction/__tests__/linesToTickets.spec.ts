import { blueprintToLines } from "../blueprintToLines";
import {
  outcomesToGuard,
  linesToTickets,
  outcomesAreCombinable,
  outcomesAreEqual,
  guardToOutcomes,
  mergeTickets,
  sortOutcomes,
  canBeCombined,
  canTicketsBeCombined,
  predictionToOutcomes,
} from "../linesToTickets";
import {
  Ticket,
  Blueprint,
  Guard,
  Line,
  Outcome,
  GuardedPrediction,
} from "../../model";

describe("canBeCombined", () => {
  it("should return false", () => {
    const ticket: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["X"], overUnders: ["U"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["1"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 4,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["U"] },
      }),
    ];

    const line: Line = [
      { matchNo: 1, outcome: "XU" },
      { matchNo: 2, outcome: "1O" },
      { matchNo: 3, outcome: "2O" },
      { matchNo: 4, outcome: "XU" },
    ];

    expect(canBeCombined(ticket, line)).toBe(false);
  });

  it("should return true", () => {
    const ticket: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X"], overUnders: ["U"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
      }),
    ];

    const line: Line = [
      { matchNo: 1, outcome: "2O" },
      { matchNo: 2, outcome: "XU" },
      { matchNo: 3, outcome: "2O" },
    ];

    expect(canBeCombined(ticket, line)).toBe(true);
  });
});

describe("canTicketsBeCombined", () => {
  it("should return false", () => {
    const ticket1: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["2"], overUnders: ["O", "U"] },
      }),
    ];
    const ticket2: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["X"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["1"], overUnders: ["O", "U"] },
      }),
    ];

    expect(canTicketsBeCombined(ticket1, ticket2)).toBe(false);
  });
});

describe("outcomesAreEqual", () => {
  it("should return true disregarding how outcomes are sorted", () => {
    const outcomes1: Outcome[] = ["2U", "XO", "1U", "1O"];
    const outcomes2: Outcome[] = ["1U", "2U", "1O", "XO"];

    expect(outcomesAreEqual(outcomes1, outcomes2)).toBe(true);
  });

  it("should return false if outcomes differs", () => {
    const outcomes1: Outcome[] = ["1U", "1O"];
    const outcomes2: Outcome[] = ["1U", "XU"];

    expect(outcomesAreEqual(outcomes1, outcomes2)).toBe(false);
  });
});

describe("outcomesAreCombinable", () => {
  it("should return true if outcomes can be combined through match outcome", () => {
    expect(outcomesAreCombinable(["1O"], ["1U"])).toBe(true);
    expect(outcomesAreCombinable(["XU"], ["1U"])).toBe(true);
    expect(outcomesAreCombinable(["1U", "XU"], ["2U"])).toBe(true);
    expect(outcomesAreCombinable(["1O", "XU"], ["1U", "XO"])).toBe(true);
    expect(outcomesAreCombinable(["1O", "2U", "1U"], ["XO", "2O", "XU"])).toBe(
      true
    );
  });

  it("should return false if outcomes CANNOT be combined through match outcome", () => {
    expect(outcomesAreCombinable(["1O"], ["XU"])).toBe(false);
    expect(outcomesAreCombinable(["XO"], ["1U"])).toBe(false);
    expect(outcomesAreCombinable(["1O", "XU"], ["1U"])).toBe(false);
    expect(outcomesAreCombinable(["1O", "XO"], ["1U", "XO"])).toBe(false);
    expect(outcomesAreCombinable(["1O", "1U"], ["XO", "2O", "XU"])).toBe(false);
    expect(outcomesAreCombinable(["XU"], ["XU", "2U"])).toBe(false);
  });
});

describe("outcomesToGuard", () => {
  it("should return 1X2 O for 1O, XO, 2O", () => {
    expect(outcomesToGuard(["1O", "XO", "2O"])).toEqual<Guard>({
      fullTimeBets: ["1", "X", "2"],
      overUnders: ["O"],
    });
  });

  it("should return 1X OU for 1O, XO, 1U, XU", () => {
    expect(outcomesToGuard(["1O", "XO", "1U", "XU"])).toEqual<Guard>({
      fullTimeBets: ["1", "X"],
      overUnders: ["O", "U"],
    });
  });

  it("should return 1 OU for 1O, 1U", () => {
    expect(outcomesToGuard(["1O", "1U"])).toEqual<Guard>({
      fullTimeBets: ["1"],
      overUnders: ["O", "U"],
    });
  });
});

describe("guardToOutcomes", () => {
  it("should return 1O, XO, 2O for 1X2 O", () => {
    expect(
      guardToOutcomes({
        fullTimeBets: ["1", "X", "2"],
        overUnders: ["O"],
      })
    ).toEqual<Outcome[]>(["1O", "XO", "2O"]);
  });

  it("should return 1O, XO, 1U, XU for 1X OU", () => {
    expect(
      guardToOutcomes({
        fullTimeBets: ["1", "X"],
        overUnders: ["O", "U"],
      })
    ).toEqual<Outcome[]>(["1O", "1U", "XO", "XU"]);
  });

  it("should return 1O, 1U for 1 OU", () => {
    expect(
      guardToOutcomes({
        fullTimeBets: ["1"],
        overUnders: ["O", "U"],
      })
    ).toEqual<Outcome[]>(["1O", "1U"]);
  });
});

describe("mergeTickets", () => {
  it("should merge two simple tickets", () => {
    const ticket1: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
    ];

    const ticket2: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["U"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
    ];

    expect(mergeTickets(ticket1, ticket2)).toEqual<Ticket>([
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["O", "U"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
    ]);
  });

  // This merge is naive and wrong, two ingoing tickets are worth 12 and the result is worth 24
  it("should merge two more complex tickets", () => {
    const ticket1: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["X"], overUnders: ["O", "U"] },
      }),
    ];

    const ticket2: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["U"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1", "2"], overUnders: ["O", "U"] },
      }),
    ];

    expect(mergeTickets(ticket1, ticket2)).toEqual<Ticket>([
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["O", "U"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
      }),
    ]);
  });
});

describe("sortOutcomes", () => {
  it("should sort outcomes", () => {
    expect(sortOutcomes(["2O", "1U", "XO", "2U"])).toEqual([
      "1U",
      "XO",
      "2O",
      "2U",
    ]);
  });
});

describe("linesToTickets", () => {
  it("should restore one single ticket", () => {
    const blueprint: Blueprint = [
      { matchNo: 1, outcomes: ["2O"] },
      { matchNo: 2, outcomes: ["XU"] },
      { matchNo: 3, outcomes: ["1O", "XO", "2O"] },
      { matchNo: 4, outcomes: ["XU", "2U"] },
      { matchNo: 5, outcomes: ["1O", "XO"] },
      { matchNo: 6, outcomes: ["1O", "XO"] },
      { matchNo: 7, outcomes: ["1O", "1U", "XO", "XU", "2O", "2U"] },
      { matchNo: 9, outcomes: ["1O", "1U", "XO", "XU", "2O", "2U"] },
    ];

    const lines: Line[] = blueprintToLines(blueprint);

    const expectedCoupon: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X"], overUnders: ["U"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 4,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["U"] },
      }),
      GuardedPrediction({
        matchNo: 5,
        guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 6,
        guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 7,
        guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
      }),
      GuardedPrediction({
        matchNo: 9,
        guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
      }),
    ];

    const tickets = linesToTickets(lines);
    expect(tickets).toEqual([expectedCoupon]);
  });

  it("should merge three simple rows into two tickets", () => {
    const lines: Line[] = [
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "XU" },
        { matchNo: 3, outcome: "1O" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "2O" },
        { matchNo: 3, outcome: "1O" },
      ],
      [
        { matchNo: 1, outcome: "1U" },
        { matchNo: 2, outcome: "XU" },
        { matchNo: 3, outcome: "1O" },
      ],
    ];

    const tickets = linesToTickets(lines);

    expect(sortTickets(tickets)).toEqual<Ticket[]>(
      sortTickets([
        [
          GuardedPrediction({
            matchNo: 1,
            guard: { fullTimeBets: ["1"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 2,
            guard: { fullTimeBets: ["2"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 3,
            guard: { fullTimeBets: ["1"], overUnders: ["O"] },
          }),
        ],
        [
          GuardedPrediction({
            matchNo: 1,
            guard: { fullTimeBets: ["1"], overUnders: ["O", "U"] },
          }),
          GuardedPrediction({
            matchNo: 2,
            guard: { fullTimeBets: ["X"], overUnders: ["U"] },
          }),
          GuardedPrediction({
            matchNo: 3,
            guard: { fullTimeBets: ["1"], overUnders: ["O"] },
          }),
        ],
      ])
    );
  });

  it("should merge rows into two tickets", () => {
    const lines: Line[] = [
      [
        { matchNo: 1, outcome: "XU" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "1O" },
        { matchNo: 4, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "XU" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "1O" },
        { matchNo: 4, outcome: "2U" },
      ],
      [
        { matchNo: 1, outcome: "XU" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "XU" },
        { matchNo: 4, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "XU" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "XU" },
        { matchNo: 4, outcome: "2U" },
      ],
      [
        { matchNo: 1, outcome: "XU" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "2O" },
        { matchNo: 4, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "XU" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "2O" },
        { matchNo: 4, outcome: "2U" },
      ],
      [
        { matchNo: 1, outcome: "2U" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "1O" },
        { matchNo: 4, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "2U" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "XU" },
        { matchNo: 4, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "2U" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "XU" },
        { matchNo: 4, outcome: "2U" },
      ],
      [
        { matchNo: 1, outcome: "2U" },
        { matchNo: 2, outcome: "1O" },
        { matchNo: 3, outcome: "2O" },
        { matchNo: 4, outcome: "XU" },
      ],
    ];

    const tickets = linesToTickets(lines);

    expect(sortTickets(tickets)).toEqual<Ticket[]>(
      sortTickets([
        [
          GuardedPrediction({
            matchNo: 1,
            guard: { fullTimeBets: ["X"], overUnders: ["U"] },
          }),
          GuardedPrediction({
            matchNo: 2,
            guard: { fullTimeBets: ["1"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 3,
            guard: { fullTimeBets: ["1", "2"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 4,
            guard: { fullTimeBets: ["X", "2"], overUnders: ["U"] },
          }),
        ],
        [
          GuardedPrediction({
            matchNo: 1,
            guard: { fullTimeBets: ["X", "2"], overUnders: ["U"] },
          }),
          GuardedPrediction({
            matchNo: 2,
            guard: { fullTimeBets: ["1"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 3,
            guard: { fullTimeBets: ["X"], overUnders: ["U"] },
          }),
          GuardedPrediction({
            matchNo: 4,
            guard: { fullTimeBets: ["X", "2"], overUnders: ["U"] },
          }),
        ],
        [
          GuardedPrediction({
            matchNo: 1,
            guard: { fullTimeBets: ["2"], overUnders: ["U"] },
          }),
          GuardedPrediction({
            matchNo: 2,
            guard: { fullTimeBets: ["1"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 3,
            guard: { fullTimeBets: ["1", "2"], overUnders: ["O"] },
          }),
          GuardedPrediction({
            matchNo: 4,
            guard: { fullTimeBets: ["X"], overUnders: ["U"] },
          }),
        ],
      ])
    );
  });

  it("should merge lines into one ticket", () => {
    const lines: Line[] = [
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "1O" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "1U" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "XO" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "2O" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "2U" },
      ],
      [
        { matchNo: 1, outcome: "XO" },
        { matchNo: 2, outcome: "1O" },
      ],
      [
        { matchNo: 1, outcome: "XO" },
        { matchNo: 2, outcome: "1U" },
      ],
      [
        { matchNo: 1, outcome: "XO" },
        { matchNo: 2, outcome: "XO" },
      ],
      [
        { matchNo: 1, outcome: "XO" },
        { matchNo: 2, outcome: "XU" },
      ],
      [
        { matchNo: 1, outcome: "XO" },
        { matchNo: 2, outcome: "2O" },
      ],
      [
        { matchNo: 1, outcome: "XO" },
        { matchNo: 2, outcome: "2U" },
      ],
    ];

    const ticket: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
      }),
    ];

    expect(linesToTickets(lines)).toEqual<Ticket[]>([ticket]);
  });
});

function sortTickets(tickets: Ticket[]): Ticket[] {
  function ticketAsString(ticket: Ticket): string {
    return ticket
      .map((gp) => `${gp.matchNo}${predictionToOutcomes(gp).join("")}`)
      .join(",");
  }

  return tickets.sort((ticket1: Ticket, ticket2: Ticket) => {
    const ticket1AsString = ticketAsString(ticket1);
    const ticket2AsString = ticketAsString(ticket2);

    if (ticket1AsString > ticket2AsString) {
      return 1;
    } else if (ticket1AsString < ticket2AsString) {
      return -1;
    } else {
      return 0;
    }
  });
}
