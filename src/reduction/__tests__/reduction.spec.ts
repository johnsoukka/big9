import { reduction } from "../reduction";
import { blueprintToLines } from "../blueprintToLines";
import { Blueprint, Line, LinePrediction, MatchNo, Outcome } from "../../model";
import { outcomeRule } from "../outcomeRule";

describe("reduction", () => {
  it("should apply the rules and reduce the lines", () => {
    const lp = (matchNo: MatchNo, outcome: Outcome): LinePrediction => ({
      matchNo,
      outcome,
    });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1O")],
      [lp(1, "1U"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "XU"), lp(2, "2O"), lp(3, "1U")],
    ];

    const reducedLines = reduction(lines, [
      outcomeRule({ overUnder: "O", min: 1 }),
      outcomeRule({ overUnder: "U", min: 2 }),
    ]);

    expect(reducedLines).toEqual([
      [lp(1, "1O"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "XU"), lp(2, "2O"), lp(3, "1U")],
    ]);
  });

  it("should calculate the correct number of rows", () => {
    const blueprint: Blueprint = [
      { matchNo: 1, outcomes: ["2O"] },
      { matchNo: 2, outcomes: ["XU"] },
      { matchNo: 3, outcomes: ["1O", "XO", "2O"] },
      { matchNo: 4, outcomes: ["XU", "2U"] },
      { matchNo: 5, outcomes: ["1O", "XO"] },
      { matchNo: 6, outcomes: ["1O", "XO"] },
      { matchNo: 7, outcomes: ["1O", "1U", "XO", "XU", "2O", "2U"] },
      { matchNo: 8, outcomes: ["1U", "XU"] },
    ];

    const lines: Line[] = blueprintToLines(blueprint);

    const reducedLines = reduction(lines, [
      outcomeRule({ overUnder: "O", min: 2 }),
      outcomeRule({ overUnder: "U", min: 2 }),
    ]);

    expect(reducedLines.length).toBe(288);
  });
});
