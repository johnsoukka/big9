import {
  blueprintToLines,
  matchPredictionToLinePredictions,
} from "../blueprintToLines";
import { Blueprint, Line, LinePrediction, MatchPrediction } from "../../model";

describe("blueprintToLines", () => {
  it("should convert a blueprint into list of lines", () => {
    const blueprint: Blueprint = [
      { matchNo: 1, outcomes: ["1O"] },
      { matchNo: 2, outcomes: ["XO", "2O"] },
    ];

    const lines: Line[] = blueprintToLines(blueprint);
    const expectedLines: Line[] = [
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "XO" },
      ],
      [
        { matchNo: 1, outcome: "1O" },
        { matchNo: 2, outcome: "2O" },
      ],
    ];

    expect(lines).toEqual(expectedLines);
  });

  it("should calculate the correct number of rows", () => {
    const blueprint: Blueprint = [
      { matchNo: 1, outcomes: ["2O"] },
      { matchNo: 2, outcomes: ["XU"] },
      { matchNo: 3, outcomes: ["1O", "XO", "2O"] },
      { matchNo: 4, outcomes: ["XU", "2U"] },
      { matchNo: 5, outcomes: ["1O", "XO"] },
      { matchNo: 6, outcomes: ["1O", "XO"] },
      { matchNo: 7, outcomes: ["1O", "1U", "XO", "XU", "2O", "2U"] },
      { matchNo: 8, outcomes: ["1U", "XU"] },
    ];

    const lines: Line[] = blueprintToLines(blueprint);

    expect(lines.length).toBe(288);
  });
});

describe("matchPredictionToLinePredictions", () => {
  it("should return all combinations of match bets and over unders", () => {
    const matchPrediction: MatchPrediction = {
      matchNo: 1,
      outcomes: ["1O", "1U", "XO", "XU"],
    };

    const linePredictions = matchPredictionToLinePredictions(matchPrediction);
    const expectedLinePredictions: LinePrediction[] = [
      { matchNo: 1, outcome: "1O" },
      { matchNo: 1, outcome: "1U" },
      { matchNo: 1, outcome: "XO" },
      { matchNo: 1, outcome: "XU" },
    ];

    expect(linePredictions).toEqual(expectedLinePredictions);
  });
});
