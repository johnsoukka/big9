import { Line, LinePrediction, MatchNo, Outcome } from "../../model";
import { isOver, isUnder, outcomeRule } from "../outcomeRule";

describe("isOver", () => {
  it("should be true for 1O, XO and 2O", () => {
    expect(isOver({ matchNo: 1, outcome: "1O" })).toBe(true);
    expect(isOver({ matchNo: 1, outcome: "1U" })).toBe(false);
    expect(isOver({ matchNo: 1, outcome: "XO" })).toBe(true);
    expect(isOver({ matchNo: 1, outcome: "XU" })).toBe(false);
    expect(isOver({ matchNo: 1, outcome: "2O" })).toBe(true);
    expect(isOver({ matchNo: 1, outcome: "2U" })).toBe(false);
  });
});

describe("isUnder", () => {
  it("should be true for 1U, XU and 2U", () => {
    expect(isUnder({ matchNo: 1, outcome: "1O" })).toBe(false);
    expect(isUnder({ matchNo: 1, outcome: "1U" })).toBe(true);
    expect(isUnder({ matchNo: 1, outcome: "XO" })).toBe(false);
    expect(isUnder({ matchNo: 1, outcome: "XU" })).toBe(true);
    expect(isUnder({ matchNo: 1, outcome: "2O" })).toBe(false);
    expect(isUnder({ matchNo: 1, outcome: "2U" })).toBe(true);
  });
});

describe("overUnderRule", () => {
  it("should filter out lines based on over", () => {
    const rule = outcomeRule({ overUnder: "O", min: 1 });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "1U"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1O")],
    ];

    const reducedLines = rule(lines);

    expect(reducedLines).toEqual([
      [lp(1, "1O"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1O")],
    ]);
  });

  it("should filter out lines based on under", () => {
    const rule = outcomeRule({ overUnder: "U", min: 2 });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1U")],
      [lp(1, "1U"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1O")],
    ];

    const reducedLines = rule(lines);

    expect(reducedLines).toEqual([[lp(1, "1U"), lp(2, "2U"), lp(3, "1U")]]);
  });

  it("should filter out lines based on fullTime", () => {
    const rule = outcomeRule({ fullTime: "1", min: 2 });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1U")],
      [lp(1, "1U"), lp(2, "2U"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2O"), lp(3, "XO")],
    ];

    const reducedLines = rule(lines);

    expect(reducedLines).toEqual([
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1U")],
      [lp(1, "1U"), lp(2, "2U"), lp(3, "1U")],
    ]);
  });

  it("should filter out lines based on fullTime and under", () => {
    const rule = outcomeRule({ fullTime: "X", overUnder: "U", min: 1 });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2O"), lp(3, "1U")],
      [lp(1, "1U"), lp(2, "XU"), lp(3, "1U")],
      [lp(1, "1O"), lp(2, "2O"), lp(3, "XO")],
    ];

    const reducedLines = rule(lines);

    expect(reducedLines).toEqual([[lp(1, "1U"), lp(2, "XU"), lp(3, "1U")]]);
  });
});

const lp = (matchNo: MatchNo, outcome: Outcome): LinePrediction => ({
  matchNo,
  outcome,
});
