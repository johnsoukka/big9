import { GuardedPrediction, Ticket, WildCard } from "../../model";
import { applyWildCard } from "../applyWildCard";

describe("applyWildCard", () => {
  it("should apply wild card to ticket on the most expensive guard", () => {
    const ticket: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["O", "U"] },
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
      }),
    ];

    const expectedTicketWithWildCard: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["2"], overUnders: ["O"] },
      }),
      WildCard({
        matchNo: 2,
      }),
      GuardedPrediction({
        matchNo: 3,
        guard: { fullTimeBets: ["1", "X"], overUnders: ["O"] },
      }),
    ];

    expect(expectedTicketWithWildCard).toEqual(applyWildCard(ticket));
  });
});
