import { Line, MatchNo, Outcome, LinePrediction } from "../../model";
import { matchOutcomeRule } from "../matchOutcomeRule";

describe("matchOutcomeRule", () => {
  it("should filter based on match and match bet", () => {
    const rule = matchOutcomeRule({
      outcomes: [
        { matchNo: 1, fullTime: "1" },
        { matchNo: 3, fullTime: "1" },
      ],
      min: 1,
    });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2O"), lp(3, "XU")],
      [lp(1, "XU"), lp(2, "1U"), lp(3, "2U")],
      [lp(1, "2O"), lp(2, "XO"), lp(3, "1O")],
    ];

    const reducedLines = rule(lines);

    expect(reducedLines).toEqual([
      [lp(1, "1O"), lp(2, "2O"), lp(3, "XU")],
      [lp(1, "2O"), lp(2, "XO"), lp(3, "1O")],
    ]);
  });

  it("should filter based on match and match bet and over under", () => {
    const rule = matchOutcomeRule({
      outcomes: [
        { matchNo: 1, fullTime: "1" },
        { matchNo: 3, overUnder: "U" },
      ],
      min: 1,
    });

    const lines: Line[] = [
      [lp(1, "1O"), lp(2, "2O"), lp(3, "XU")],
      [lp(1, "XU"), lp(2, "1U"), lp(3, "2U")],
      [lp(1, "2O"), lp(2, "XO"), lp(3, "1O")],
    ];

    const reducedLines = rule(lines);

    expect(reducedLines).toEqual([
      [lp(1, "1O"), lp(2, "2O"), lp(3, "XU")],
      [lp(1, "XU"), lp(2, "1U"), lp(3, "2U")],
    ]);
  });
});

const lp = (matchNo: MatchNo, outcome: Outcome): LinePrediction => ({
  matchNo,
  outcome,
});
