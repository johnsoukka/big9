import { Line } from "../model";

export function reduction(lines: Line[], rules: ReductionRule[]) {
  let filteredLines = [...lines];

  rules.forEach((rule) => {
    filteredLines = rule(filteredLines);
  });

  return filteredLines;
}

export interface ReductionRule {
  (lines: Line[]): Line[];
}
