import { Ticket, WildCard } from "../model";

export function applyWildCard(ticketIn: Ticket): Ticket {
  const ticket = Array.from(ticketIn);
  const sortedByOutcomesLength = ticket.sort(
    (p1, p2) => p2.hash.length - p1.hash.length
  );

  const [mostCostlyPrediction] = sortedByOutcomesLength.splice(0, 1);
  const ticketWithWildCard = [
    WildCard({ matchNo: mostCostlyPrediction.matchNo }),
    ...sortedByOutcomesLength,
  ];

  return ticketWithWildCard.sort((p1, p2) => p1.matchNo - p2.matchNo);
}
