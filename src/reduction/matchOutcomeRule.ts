import { ReductionRule } from "./reduction";
import { Line, FullTime, MatchNo, OverUnder } from "../model";
import { outcomeReductionFactory } from "./outcomeReductionFactory";

export const matchOutcomeRule =
  (config: MatchOutcomeRuleConfig): ReductionRule =>
  (lines: Line[]) => {
    return lines.filter((line) => {
      let noOfPredictionsSatisfied = config.outcomes.filter((matchOutcome) => {
        const linePrediction = line.find(
          (lp) => lp.matchNo === matchOutcome.matchNo
        );
        const reductionFunc = outcomeReductionFactory(
          matchOutcome.fullTime,
          matchOutcome.overUnder
        );
        return (linePrediction && reductionFunc(linePrediction)) || false;
      }).length;

      return (
        noOfPredictionsSatisfied >= (config.min ?? 0) &&
        noOfPredictionsSatisfied <= (config.max ?? Number.MAX_SAFE_INTEGER)
      );
    });
  };

// TODO: Change this to be rule = fullTime | overUnder | outcome
interface MatchOutcome {
  matchNo: MatchNo;
  fullTime?: FullTime;
  overUnder?: OverUnder;
}

export interface MatchOutcomeRuleConfig {
  outcomes: MatchOutcome[];
  min?: number;
  max?: number;
}
