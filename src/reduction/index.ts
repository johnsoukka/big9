import { Blueprint, Ticket } from "../model";
import { applyWildCard } from "./applyWildCard";
import { blueprintToLines } from "./blueprintToLines";
import { linesToTickets } from "./linesToTickets";
import { reduction, ReductionRule } from "./reduction";

export function reduceBlueprint(
  blueprint: Blueprint,
  rules: ReductionRule[]
): Ticket[] {
  const lines = blueprintToLines(blueprint);
  console.log(
    `Starting reduction of blueprint with a total of ${lines.length} lines`
  );
  const reducedLines = reduction(lines, rules);
  console.log(`Blueprint reduced to ${reducedLines.length} lines`);

  const tickets = linesToTickets(reducedLines);
  const ticketsWithWildCard = tickets.map(applyWildCard);

  return ticketsWithWildCard;
}

export { ReductionRule } from "./reduction";
export { matchOutcomeRule } from "./matchOutcomeRule";
export { outcomeRule } from "./outcomeRule";
