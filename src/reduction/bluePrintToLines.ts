import { Blueprint, Line, LinePrediction, MatchPrediction } from "../model";
import { cartesian } from "./cartesian";

export function blueprintToLines(blueprint: Blueprint): Line[] {
  const product = cartesian(...blueprint.map(matchPredictionToLinePredictions));

  return product;
}

export function matchPredictionToLinePredictions(
  matchPrediction: MatchPrediction
): LinePrediction[] {
  return matchPrediction.outcomes.flatMap((outcome) => {
    return {
      matchNo: matchPrediction.matchNo,
      outcome: outcome,
    };
  });
}
