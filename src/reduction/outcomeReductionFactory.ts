import { LinePrediction, FullTime, Outcome, OverUnder } from "../model";

type ReductionFunction = (linePrediction: LinePrediction) => boolean;

export function outcomeReductionFactory(
  fullTime?: FullTime,
  overUnder?: OverUnder
): ReductionFunction {
  if (fullTime && overUnder) {
    return (linePrediction: LinePrediction) =>
      linePrediction.outcome === `${fullTime}${overUnder}`;
  } else if (fullTime) {
    const outcomesToInclude: Outcome[] = (fullTime === "1" && ["1O", "1U"]) ||
      (fullTime === "X" && ["XO", "XU"]) || ["2O", "2U"];
    return (linePrediction: LinePrediction) =>
      outcomesToInclude.includes(linePrediction.outcome);
  } else if (overUnder) {
    return overUnder === "O" ? isOver : isUnder;
  } else {
    return (_: LinePrediction) => true;
  }
}

export function isOver(linePrediction: LinePrediction): boolean {
  return ["1O", "XO", "2O"].includes(linePrediction.outcome);
}

export function isUnder(linePrediction: LinePrediction): boolean {
  return ["1U", "XU", "2U"].includes(linePrediction.outcome);
}
