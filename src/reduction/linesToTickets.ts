import {
  Ticket,
  Guard,
  Line,
  FullTime,
  Outcome,
  OverUnder,
  TicketPrediction,
  isGuardedPrediction,
  GuardedPrediction,
} from "../model";

const logSteps = [0.25, 0.5, 0.75];
const ticketLogSteps = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];

export function linesToTickets(linesIn: Line[]): Ticket[] {
  console.log(`Start merging ${linesIn.length} lines into tickets`);
  const lines = Array.from(linesIn);
  if (lines.length === 0) {
    return [];
  }

  const tickets: Ticket[] = [];

  let lineIndex = -1;
  do {
    if ((linesIn.length - lines.length) / linesIn.length > logSteps[0]) {
      console.log(
        `Has processed ${logSteps.splice(0, 1)[0] * 100} percent of ${
          linesIn.length
        } total lines`
      );
    }
    lineIndex = lines.findIndex((line1, index1) =>
      lines.some(
        (line2, index2) =>
          index1 !== index2 && canBeCombined(lineToTicket(line1), line2)
      )
    );

    if (lineIndex > -1) {
      const [firstLine] = lines.splice(lineIndex, 1);
      let ticket = lineToTicket(firstLine);

      let mergableLine;
      do {
        mergableLine = lines.find((line) => canBeCombined(ticket, line));

        if (mergableLine) {
          const index = lines.indexOf(mergableLine);
          lines.splice(index, 1);
          ticket = mergeLineIntoTicket(ticket, mergableLine);
        }
      } while (mergableLine);

      tickets.push(ticket);
    }
  } while (lineIndex > -1);

  let ticketIndex = -1;
  let safetyCount = 0;
  let max = tickets.length * 2;

  do {
    if (safetyCount / max > ticketLogSteps[0]) {
      console.log(
        `Has processed ${
          ticketLogSteps.splice(0, 1)[0] * 100
        } percent of max ${max}`
      );
    }
    safetyCount++;
    ticketIndex = tickets.findIndex((ticket1, index1) =>
      tickets.some(
        (ticket2, index2) =>
          index1 !== index2 && canTicketsBeCombined(ticket1, ticket2)
      )
    );

    if (ticketIndex > -1) {
      const [firstTicket] = tickets.splice(ticketIndex, 1);
      const mergeableTicketIndex = tickets.findIndex((ticket) =>
        canTicketsBeCombined(firstTicket, ticket)
      );

      if (mergeableTicketIndex > -1) {
        const [mergeableCoupon] = tickets.splice(mergeableTicketIndex, 1);
        const newCoupon = mergeTickets(firstTicket, mergeableCoupon);
        tickets.push(newCoupon);
      }
    }
  } while (ticketIndex > -1 && safetyCount < max);

  lines.forEach((line) => tickets.push(lineToTicket(line)));

  return tickets;
}

export function lineToTicket(line: Line): Ticket {
  return line.map((prediction) =>
    GuardedPrediction({
      matchNo: prediction.matchNo,
      guard: outcomesToGuard([prediction.outcome]),
    })
  );
}

export function canBeCombined(ticket: Ticket, line: Line): boolean {
  return canTicketsBeCombined(ticket, lineToTicket(line));
}

export function canTicketsBeCombined(
  ticket1: Ticket,
  ticket2: Ticket
): boolean {
  const nonEqualPredictions = ticket1.filter((prediction) => {
    return (
      predictionsAreEqual(prediction, correspondingPrediction(prediction)) ===
      false
    );
  });

  if (nonEqualPredictions.length === 1) {
    const nonEqualPrediction = nonEqualPredictions[0];

    return ticketsAreCombinable(
      nonEqualPrediction,
      correspondingPrediction(nonEqualPrediction)
    );
  } else {
    return false;
  }

  function correspondingPrediction(prediction: TicketPrediction) {
    const correspondingPrediction = ticket2.find(
      (p) => p.matchNo === prediction.matchNo
    );
    if (!correspondingPrediction) {
      throw new Error("Missing corresponding prediction in other ticket");
    }
    return correspondingPrediction;
  }
}

export let areEqualDuration = 0;
function predictionsAreEqual(
  prediction1: TicketPrediction,
  prediction2: TicketPrediction
): boolean {
  const start = Date.now();
  const res = prediction1.hash === prediction2.hash;
  const end = Date.now();
  areEqualDuration += end - start;
  return res;
}

function ticketsAreCombinable(
  prediction1: TicketPrediction,
  prediction2: TicketPrediction
): boolean {
  return outcomesAreCombinable(
    predictionToOutcomes(prediction1),
    predictionToOutcomes(prediction2)
  );
}

export function outcomesAreEqual(
  outcomes1: Outcome[],
  outcomes2: Outcome[]
): boolean {
  return [...outcomes1].sort().toString() === [...outcomes2].sort().toString();
}

export function outcomesAreCombinable(
  outcomes1: Outcome[],
  outcomes2: Outcome[]
): boolean {
  const totalOutcomes = outcomes1.length + outcomes2.length;
  const unique = uniqueOutcomes(outcomes1, outcomes2);
  const guard = outcomesToGuard(unique);
  const outcomes = guardToOutcomes(guard);

  return totalOutcomes === unique.length && outcomesAreEqual(outcomes, unique);
}

export function uniqueOutcomes(
  outcomes1: Outcome[],
  outcomes2: Outcome[]
): Outcome[] {
  return [...new Set(outcomes1.concat(outcomes2))];
}

export function outcomesToGuard(outcomes: Outcome[]): Guard {
  const fullTimeBets = [...new Set(outcomes.map((o) => o[0] as FullTime))];
  const overUnders = [...new Set(outcomes.map((o) => o[1] as OverUnder))];

  return {
    fullTimeBets,
    overUnders,
  };
}

export function guardToOutcomes(guard: Guard): Outcome[] {
  return guard.fullTimeBets.flatMap((m) => {
    return guard.overUnders.map((ou) => `${m}${ou}` as Outcome);
  });
}

export function predictionToOutcomes(prediction: TicketPrediction): Outcome[] {
  if (isGuardedPrediction(prediction)) {
    return prediction.guard.fullTimeBets.flatMap((m) => {
      return prediction.guard.overUnders.map((ou) => `${m}${ou}` as Outcome);
    });
  } else {
    return ["1O", "1U", "XO", "XU", "2O", "2U"];
  }
}

export function mergeLineIntoTicket(ticket: Ticket, line: Line): Ticket {
  return mergeTickets(ticket, lineToTicket(line));
}

export function mergeTickets(ticket1: Ticket, ticket2: Ticket): Ticket {
  return ticket1.map((guardedPrediction) => {
    const correspondingPrediction = ticket2.find(
      (c) => c.matchNo === guardedPrediction.matchNo
    );
    if (!correspondingPrediction) {
      throw new Error("No corresponding prediction found");
    }

    const outcomes1 = predictionToOutcomes(guardedPrediction);
    const outcomes2 = predictionToOutcomes(correspondingPrediction);
    const sortedOutcomes = sortOutcomes(outcomes1.concat(outcomes2));

    return GuardedPrediction({
      matchNo: guardedPrediction.matchNo,
      guard: outcomesToGuard(sortedOutcomes),
    });
  });
}

export const sortOutcomes = (outcomes: Outcome[]): Outcome[] => {
  const list: Outcome[] = ["1O", "1U", "XO", "XU", "2O", "2U"];

  return outcomes.sort((o1, o2) => {
    if (list.indexOf(o1) > list.indexOf(o2)) {
      return 1;
    } else if (list.indexOf(o1) < list.indexOf(o2)) {
      return -1;
    } else {
      return 0;
    }
  });
};
