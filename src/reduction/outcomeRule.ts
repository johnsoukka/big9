import { ReductionRule } from "./reduction";
import { Line, LinePrediction, FullTime, OverUnder } from "../model";
import { outcomeReductionFactory } from "./outcomeReductionFactory";

export const outcomeRule =
  (config: OutcomeRuleConfig): ReductionRule =>
  (lines: Line[]) => {
    return lines.filter((line) => {
      const reductionFunc = outcomeReductionFactory(
        config.fullTime,
        config.overUnder
      );

      const lineCountMatching = line.filter(reductionFunc).length;

      return (
        lineCountMatching >= (config.min ?? 0) &&
        lineCountMatching <= (config.max ?? Number.MAX_SAFE_INTEGER)
      );
    });
  };

export function isOver(linePrediction: LinePrediction): boolean {
  return ["1O", "XO", "2O"].includes(linePrediction.outcome);
}

export function isUnder(linePrediction: LinePrediction): boolean {
  return ["1U", "XU", "2U"].includes(linePrediction.outcome);
}

// TODO: Change this to be rule = fullTime | overUnder | outcome
export interface OutcomeRuleConfig {
  fullTime?: FullTime;
  overUnder?: OverUnder;
  min?: number;
  max?: number;
}
