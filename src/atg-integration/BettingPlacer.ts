import { Ticket } from "../model";
import { fetchNextGameday } from "./fetchNextGameday";
import { makeTicketPayload } from "./makeTicketPayload";
import { IGameday, ITicketPayload } from "./models";
import { v4 as uuidV4 } from "uuid";
import axios, { AxiosRequestHeaders } from "axios";

export class BettingPlacer {
  private gameday?: IGameday;
  private readonly authorization: string;

  constructor(authorization: string) {
    this.authorization = authorization;
  }

  async init() {
    this.gameday = await fetchNextGameday();
  }

  async placeTicket(ticket: Ticket): Promise<void> {
    if (!this.gameday) {
      throw new Error("Betting placer not initialised");
    }

    const ticketUuid = uuidV4();

    const ticketPayload = makeTicketPayload(ticketUuid, this.gameday)(ticket);
    await this.postTicket(ticketPayload);
  }

  private async postTicket(payload: ITicketPayload): Promise<void> {
    const url =
      "https://www.atg.se/services/tokenized/sports-coupon/api/v1/coupons";
    const headers: AxiosRequestHeaders = {
      "x-requested-by": "ATG",
      "content-type": "application/json",
      accept: "application/json",
      Origin: "https://www.atg.se",
      authorization: `Bearer ${this.authorization}`,
    };

    await axios.post(url, payload, { headers });
  }
}
