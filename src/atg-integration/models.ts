export interface ITicketPayload {
  id: string;
  gameId: string;
  gameType: "BIG9";
  gameDate: string; // datetime
  modified: string; // datetime
  isPublic: false;
  coupon: ICouponPayload;
}

interface ICouponPayload {
  version: 1;
  rowCost: 50;
  wildcard?: string;
  harryStake: 0;
  outcomes: { [key: string]: IOutcomePayload };
}

type FullTime = "home" | "tie" | "away";
type OverUnder = "over" | "under";

export interface IOutcomePayload {
  fullTime: FullTime[];
  overUnder: OverUnder[];
  harry: false;
  matchId: string;
}

export interface IGameday {
  id: string;
  gameType: string;
  startTime: string;
  matches: IMatch[];
}

export interface IMatch {
  id: string;
  number: string;
}
