import { IGameday, IMatch } from "./models";
import axios from "axios";

export function parseGamedayResponse(json: any): IGameday {
  const gamedayJson = (json as any[]).find((g) => g.status === "SELL_OPEN");
  const { id, gameType, startTime, matches } = gamedayJson;

  if (![id, gameType, startTime, matches].every((value) => !!value)) {
    throw new Error("Invalid response, some field(s) are missing from gameday");
  }

  if (![id, gameType, startTime].every((value) => typeof value === "string")) {
    throw new Error("Invalid type of response field(s)");
  }

  if (gameType !== "BIG9") {
    throw new Error("Invalid game type");
  }

  if (!Array.isArray(matches) || matches.length !== 9) {
    throw new Error(
      "Invalid response, matches are expected to be an array of length 9"
    );
  }

  return {
    id,
    gameType,
    startTime,
    matches: matches.map(parseMatch),
  };
}

function parseMatch(match: any): IMatch {
  const { id, number } = match;

  if (![id, number].every((value) => !!value)) {
    throw new Error("Invalid response, some field(s) are missing from match");
  }

  if (![id, number].every((value) => typeof value === "string")) {
    throw new Error("Invalid type of response field(s)");
  }

  return {
    id,
    number,
  };
}

export async function fetchNextGameday(): Promise<IGameday> {
  const today = new Date().toISOString().substring(0, 10);
  const url = `https://www.atg.se/services/sports-info-offering/api/v1/offering?status=SELL_OPEN&fromDate=${today}`;
  const response = await axios.get(url);

  return parseGamedayResponse(response.data);
}
