import {
  FullTime,
  isGuardedPrediction,
  OverUnder,
  Ticket,
  TicketPrediction,
} from "../model";
import { IGameday, IOutcomePayload, ITicketPayload } from "./models";

interface IMakeTicketPayload {
  (ticket: Ticket): ITicketPayload;
}

export function makeTicketPayload(
  uuid: string,
  gameday: IGameday
): IMakeTicketPayload {
  return (ticket: Ticket) => {
    return {
      id: uuid,
      gameId: gameday.id,
      gameType: "BIG9",
      gameDate: gameday.startTime,
      modified: new Date().toISOString(),
      isPublic: false,
      coupon: {
        version: 1,
        rowCost: 50,
        wildcard: wildCardId(uuid, gameday, ticket),
        harryStake: 0,
        outcomes: Object.fromEntries(
          ticket.map(ticketPredictionToOutcomePayload(uuid, gameday))
        ),
      },
    };
  };
}

export const ticketPredictionToOutcomePayload =
  (uuid: string, gameday: IGameday) =>
  (prediction: TicketPrediction): [string, IOutcomePayload] => {
    const match = gameday.matches.find(
      (m) => m.number === prediction.matchNo.toString()
    );

    if (!match) {
      throw new Error("Match not found");
    }

    const outcomeKey = `${uuid}-${match.id}`;

    if (isGuardedPrediction(prediction)) {
      return [
        outcomeKey,
        {
          fullTime: prediction.guard.fullTimeBets.map(mapFullTime),
          overUnder: prediction.guard.overUnders.map(mapOverUnder),
          harry: false,
          matchId: match.id,
        },
      ];
    } else {
      // Wild card
      return [
        outcomeKey,
        {
          fullTime: [],
          overUnder: [],
          harry: false,
          matchId: match.id,
        },
      ];
    }
  };

function wildCardId(uuid: string, gameday: IGameday, ticket: Ticket): string {
  const wildCardGame = ticket.find((p) => p.isWildCard);

  if (!wildCardGame) {
    throw new Error("No wild card found");
  }

  const match = gameday.matches.find(
    (m) => m.number === wildCardGame.matchNo.toString()
  );

  if (!match) {
    throw new Error("Match not found");
  }

  return `${uuid}-${match.id}`;
}

function mapFullTime(fullTime: FullTime) {
  if (fullTime === "1") {
    return "home";
  } else if (fullTime === "X") {
    return "tie";
  } else {
    return "away";
  }
}

function mapOverUnder(overUnder: OverUnder) {
  if (overUnder === "O") {
    return "over";
  } else {
    return "under";
  }
}
