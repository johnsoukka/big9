import { parseGamedayResponse } from "../fetchNextGameday";
import fs from "fs";
import path from "path";
import { IGameday } from "../models";

describe("parseGamedayResponse", () => {
  it("should parse snapshot data", () => {
    const response = fs.readFileSync(
      path.join(__dirname, "nextGamedayResponseSnaphot.json"),
      "utf-8"
    );
    const gameday = parseGamedayResponse(JSON.parse(response));

    expect(gameday).toEqual<IGameday>({
      id: "BIG9_2022-04-09_1154070178_497161608",
      gameType: "BIG9",
      startTime: "2022-04-09T14:00:00Z",
      matches: [
        {
          id: "2022-04-09_FOOTBALL_MATCH_6b44c9bc-f3de-4e88-b9e3-871ec0d843d2",
          number: "1",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_5204662f-a02a-4f79-b699-6cc0682cd26c",
          number: "2",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_1dd60905-6955-4cdc-934d-fd7b6f6d18fd",
          number: "3",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_c5cee7de-f260-4a69-a43b-b6be2350c20e",
          number: "4",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_3184afaa-d0ac-4f9d-9787-b46c6b2279d4",
          number: "5",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_9f17477d-8bf0-426a-ad68-8c03dd46bb2f",
          number: "6",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_b3fc13ef-75c7-4040-a0bd-726e52a1ecde",
          number: "7",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_ef1d9d76-c4a3-49e5-b5e0-3863af2dfe85",
          number: "8",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_0bb513f2-a4a3-4647-850d-d32205f30671",
          number: "9",
        },
      ],
    });
  });
});
