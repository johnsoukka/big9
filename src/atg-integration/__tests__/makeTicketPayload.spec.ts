import { GuardedPrediction, Ticket, WildCard } from "../../model";
import { makeTicketPayload } from "../makeTicketPayload";
import { IGameday, ITicketPayload } from "../models";

describe("makeTicketPayload", () => {
  it("should create the expected payload from gameday and ticket", () => {
    const ticket: Ticket = [
      GuardedPrediction({
        matchNo: 1,
        guard: { fullTimeBets: ["1"], overUnders: ["O", "U"] },
      }),
      GuardedPrediction({
        matchNo: 2,
        guard: { fullTimeBets: ["X", "2"], overUnders: ["O"] },
      }),
      WildCard({
        matchNo: 3,
      }),
      GuardedPrediction({
        matchNo: 4,
        guard: { fullTimeBets: ["1", "X", "2"], overUnders: ["O", "U"] },
      }),
    ];

    const gameday: IGameday = {
      id: "BIG9_2022-04-09_1154070178_497161608",
      gameType: "BIG9",
      startTime: "2022-04-09T14:00:00Z",
      matches: [
        {
          id: "2022-04-09_FOOTBALL_MATCH_6b44c9bc-f3de-4e88-b9e3-871ec0d843d2",
          number: "1",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_5204662f-a02a-4f79-b699-6cc0682cd26c",
          number: "2",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_1dd60905-6955-4cdc-934d-fd7b6f6d18fd",
          number: "3",
        },
        {
          id: "2022-04-09_FOOTBALL_MATCH_c5cee7de-f260-4a69-a43b-b6be2350c20e",
          number: "4",
        },
      ],
    };

    const uuid = "5287da65-deab-4590-9d18-989632178019";

    expect(makeTicketPayload(uuid, gameday)(ticket)).toEqual<ITicketPayload>({
      id: "5287da65-deab-4590-9d18-989632178019",
      gameId: "BIG9_2022-04-09_1154070178_497161608",
      gameType: "BIG9",
      gameDate: "2022-04-09T14:00:00Z",
      modified: expect.any(String),
      isPublic: false,
      coupon: {
        version: 1,
        rowCost: 50,
        wildcard:
          "5287da65-deab-4590-9d18-989632178019-2022-04-09_FOOTBALL_MATCH_1dd60905-6955-4cdc-934d-fd7b6f6d18fd",
        harryStake: 0,
        outcomes: {
          "5287da65-deab-4590-9d18-989632178019-2022-04-09_FOOTBALL_MATCH_6b44c9bc-f3de-4e88-b9e3-871ec0d843d2":
            {
              fullTime: ["home"],
              overUnder: ["over", "under"],
              harry: false,
              matchId:
                "2022-04-09_FOOTBALL_MATCH_6b44c9bc-f3de-4e88-b9e3-871ec0d843d2",
            },
          "5287da65-deab-4590-9d18-989632178019-2022-04-09_FOOTBALL_MATCH_5204662f-a02a-4f79-b699-6cc0682cd26c":
            {
              fullTime: ["tie", "away"],
              overUnder: ["over"],
              harry: false,
              matchId:
                "2022-04-09_FOOTBALL_MATCH_5204662f-a02a-4f79-b699-6cc0682cd26c",
            },
          "5287da65-deab-4590-9d18-989632178019-2022-04-09_FOOTBALL_MATCH_1dd60905-6955-4cdc-934d-fd7b6f6d18fd":
            {
              fullTime: [],
              overUnder: [],
              harry: false,
              matchId:
                "2022-04-09_FOOTBALL_MATCH_1dd60905-6955-4cdc-934d-fd7b6f6d18fd",
            },
          "5287da65-deab-4590-9d18-989632178019-2022-04-09_FOOTBALL_MATCH_c5cee7de-f260-4a69-a43b-b6be2350c20e":
            {
              fullTime: ["home", "tie", "away"],
              overUnder: ["over", "under"],
              harry: false,
              matchId:
                "2022-04-09_FOOTBALL_MATCH_c5cee7de-f260-4a69-a43b-b6be2350c20e",
            },
        },
      },
    });
  });
});
