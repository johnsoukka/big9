// @ts-nocheck
import { BettingPlacer } from "./atg-integration/BettingPlacer";
import { ticketsCost } from "./cost";
import { Blueprint } from "./model";
import {
  matchOutcomeRule,
  outcomeRule,
  reduceBlueprint,
  ReductionRule,
} from "./reduction";
import { areEqualDuration } from "./reduction/linesToTickets";
import fs from "fs";
import path from "path";
import puppeteer from "puppeteer";

const main = () => {
  /*
      1. Leicester - Aston Villa
      2. Manchester City - Watford
      3. Norwich - Newcastle
      4. Brentford - Tottenham
      5. Bayern - Dortmund
      6. Inter - Roma
      7. Hellas - Samp
      8. Lyon - Montpellier
      9. St Etienne - Monaco
    */
  const blueprint: Blueprint = [
    { matchNo: 1, outcomes: ["2U", "2O"] },
    { matchNo: 2, outcomes: ["1O", "1U"] },
    { matchNo: 3, outcomes: ["1O", "1U", "XU", "2O"] },
    { matchNo: 4, outcomes: ["2O"] },
    { matchNo: 5, outcomes: ["1O", "2O"] },
    { matchNo: 6, outcomes: ["1O", "1U", "XU", "2O"] },
    { matchNo: 7, outcomes: ["1O", "1U", "XU", "XO", "2U", "2O"] },
    { matchNo: 8, outcomes: ["1O", "1U", "XU"] },
    { matchNo: 9, outcomes: ["XU", "2O", "2U"] },
  ];

  const rules: ReductionRule[] = [
    outcomeRule({ fullTime: "X", overUnder: "U", min: 1 }),
    // outcomeRule({ overUnder: "O", min: 3 }),
    // outcomeRule({ overUnder: "U", min: 2 }),
    // outcomeRule({ fullTime: "1", min: 2 }),
    // outcomeRule({ fullTime: "2", min: 3 }),
    // matchOutcomeRule({
    //   outcomes: [
    //     { matchNo: 6, fullTime: "1" },
    //     // { matchNo: 5, fullTime: "1" },
    //     // { matchNo: 6, fullTime: "1" },
    //     { matchNo: 7, fullTime: "1" },
    //   ],
    //   max: 1,
    // }),
  ];

  const start = Date.now();
  const tickets = reduceBlueprint(blueprint, rules);
  const finished = Date.now();

  console.log("Cost", ticketsCost(tickets));
  console.log("No of tickets", tickets.length);
  console.log(`Duration: ${(finished - start) / 1000} seconds`);
  console.log(`Are equal: ${areEqualDuration / 1000} seconds`);

  // fs.writeFileSync(
  //   path.join(__dirname, "tickets3.json"),
  //   JSON.stringify(tickets),
  //   "utf-8"
  // );

  return tickets;
};

// const ticketCost = (ticket: Ticket): number => {
//   return ticket.reduce<number>((sum, gp) => {
//     return sum * guardToOutcomes(gp.guard).length;
//   }, 1);
// };

// TODO: räkna kostnad
// TODO: lägg på ett wild card, se om det går att slå ihop lite mer och även om det går att få ner kuponger mot liten kostnad.

// @ts-ignore
async function main2() {
  const ticketsFromMain = JSON.parse(
    fs.readFileSync(path.join(__dirname, "tickets3.json"), "utf-8")
  );
  const tickets = ticketsFromMain;
  console.log(tickets.length);
  const placer = new BettingPlacer("0401133f-64fd-4abe-b1b5-3e0ee3a14c1b");
  await placer.init();
  for (const ticket of tickets) {
    await placer.placeTicket(ticket);
  }
}

// main();
// main2().then(() => console.log("Finished!"));

async function main3() {
  const wsChromeEndpointurl =
    "ws://127.0.0.1:9222/devtools/browser/fa77fb25-37c4-4bcd-8a70-f298aa782560";
  const browser = await puppeteer.connect({
    browserWSEndpoint: wsChromeEndpointurl,
  });

  const pages = await browser.pages();

  console.log("Pages", pages.length);
  const page = pages.find((p) => /atg\.se/gi.test(p.url()));
  console.log("URL", page.url());
  // await page.reload({ waitUntil: "networkidle0" });

  const betslipToggle = await page.$('[data-test-id="big9-betslip-toggle"]');

  if (betslipToggle) {
    await betslipToggle.click();
    const placeBetButton = await page?.$(
      '[data-test-id="big9-betslip-place-bet"]'
    );

    if (placeBetButton) {
      await placeBetButton.click();
      await page.reload({ waitUntil: "networkidle0" });
    } else {
      console.log("No place bet button found!");
    }
  } else {
    console.log("No betslip toggle found!");
  }
}

// main3().then(() => {
//   console.log("Finished!");
//   process.exit(0);
// });
