import { guardToOutcomes, sortOutcomes } from "../reduction/linesToTickets";

export type MatchNo = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9;
export type FullTime = "1" | "X" | "2";
export type OverUnder = "O" | "U";
export type Outcome = "1O" | "1U" | "XO" | "XU" | "2O" | "2U";

export interface MatchPrediction {
  matchNo: MatchNo;
  outcomes: Outcome[];
}

export interface LinePrediction {
  matchNo: MatchNo;
  outcome: Outcome;
}

export interface Guard {
  fullTimeBets: FullTime[];
  overUnders: OverUnder[];
}

export interface IWildCard {
  matchNo: MatchNo;
  // guard: { fullTimeBets: ["1", "X", "2"]; overUnders: ["O", "U"] };
  isWildCard: true;
  hash: "1O1UXOXU2O2U";
}

export function WildCard({ matchNo }: { matchNo: MatchNo }): IWildCard {
  return { matchNo, isWildCard: true, hash: "1O1UXOXU2O2U" };
}

export interface IGuardedPrediction {
  matchNo: MatchNo;
  guard: Guard;
  hash: string; // TODO: hash is a bad name
  isWildCard: false;
}

export function GuardedPrediction({
  matchNo,
  guard,
}: Omit<IGuardedPrediction, "hash" | "isWildCard">): IGuardedPrediction {
  return {
    matchNo,
    guard,
    hash: sortOutcomes(guardToOutcomes(guard)).join(""),
    isWildCard: false,
  };
}

export function isGuardedPrediction(
  prediction: TicketPrediction
): prediction is IGuardedPrediction {
  return (prediction as IGuardedPrediction).guard != undefined;
}

export type TicketPrediction = IGuardedPrediction | IWildCard;

export type Blueprint = MatchPrediction[];
export type Line = LinePrediction[];
export type Ticket = TicketPrediction[];
