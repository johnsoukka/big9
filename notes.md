# Notes

### Ramen

### Reducering

- Minst två kryss
- Minst ett kryss under (kanske båda)
- Minst två över och två under
- Av dessa matcher ska min/max utfall inträffa. Tex av match 1 (hemmaseger), 4 (bortaseger) och 6 (överspel) ska minst en bli en bli som vi tänkt.
- Reducera bort favoritrader (antingen ett visst antal/procent eller om den och den går in så måste x skrällar gå in, typ)
- Ovan kommer man kunna lösa ganska bra (och kontrollerat) genom `matchOutcome` och säga dessa teckan får max inträffa x.
- Reducer bort omöjliga skrällar? Samma.

### Övrigt

- Jämför mot bettingodds och hitta värden
- "Integrationstest". Ta ett system, gör om till rader. Ta alla rader och behåll. Merga till kuponger. Testa att alla rader är en rätt rad på en kupong.
